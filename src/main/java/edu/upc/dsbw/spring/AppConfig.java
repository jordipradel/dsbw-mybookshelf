package edu.upc.dsbw.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import edu.upc.dsbw.spring.business.BooksController;

@Configuration
public class AppConfig {

	@Bean
	public BooksController getBooksController(){
		BooksController booksController = new BooksController();
		long martinFowlerId = booksController.addAuthor("Martin", "Fowler");
		booksController.addBook(martinFowlerId, "Patterns of Enterprise Application Architecture", 2003);
		booksController.addBook(martinFowlerId, "Refactoring: Improving the Design of Existing Code", 1999);
		booksController.addBook(martinFowlerId, "UML distilled: a brief guide to the standard object modeling language", 2004);
		long kentBeckId = booksController.addAuthor("Kent", "Beck");
		booksController.addBook(kentBeckId, "Test-driven Development by example", 2003);
		return booksController;
	}
}
