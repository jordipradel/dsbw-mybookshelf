package edu.upc.dsbw.spring.business;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.upc.dsbw.spring.business.model.Author;
import edu.upc.dsbw.spring.business.model.Book;

public class BooksController {

	private long nextBookId = 0;
	private long nextAuthorId = 0;
	private Map<Long, Book> booksDB = new HashMap<Long, Book>();
	private Map<Long, Author> authorsDB = new HashMap<Long, Author>();

	public enum ListBooksOrderBy {
		insertionDate, title, author, publicationYear
	}

	public List<Book> listBooks(final ListBooksOrderBy orderBy) {
		List<Book> books = new LinkedList<Book>(booksDB.values());
		if (orderBy != ListBooksOrderBy.insertionDate) {
			Collections.sort(books, new Comparator<Book>() {

				public int compare(Book b1, Book b2) {
					switch (orderBy) {
					case title:
						return b1.getTitle().compareTo(b2.getTitle());
					case author:
						return b1.getAuthor().getName()
								.compareTo(b2.getAuthor().getName());
					case publicationYear:
						return b2.getPublicationYear() - b1.getPublicationYear();
					default:
						throw new IllegalArgumentException(
								"Unknown order by criteria " + orderBy);
					}
				}
			});
		}
		return books;
	}

	public Book getBook(Long id) {
		return booksDB.get(id);
	}

	public void addBook(Long authorId, String title, int publicationYear) {
		Author author = getAuthor(authorId);
		if (author == null)
			throw new IllegalArgumentException("Unknown author " + authorId);
		Book result = author.newBook(title, publicationYear);
		result.setId(nextBookId++);
		booksDB.put(result.getId(), result);
	}

	public void removeBook(Long id) {
		Book book = getBook(id);
		book.remove();
		booksDB.remove(book);
	}

	public void updateBook(Long id, Long authorId, String title,
			int publicationYear) {
		Book book = getBook(id);
		if (book == null)
			throw new IllegalArgumentException("Unknown book " + id);
		Author author = getAuthor(authorId);
		if (author == null)
			throw new IllegalArgumentException("Unknown author " + authorId);
		book.update(author, title, publicationYear);
	}

	public List<Author> listAuthors() {
		List<Author> authors = new LinkedList<Author>(authorsDB.values());
		Collections.sort(authors, new Comparator<Author>() {

			public int compare(Author a1, Author a2) {
				return a1.getName().compareTo(a2.getName());
			}
		});
		return authors;
	}

	public Author getAuthor(Long id) {
		return authorsDB.get(id);
	}

	public long addAuthor(String firstName, String lastName) {
		long id = nextAuthorId++;
		authorsDB.put(id, new Author(firstName, lastName));
		return id;
	}
	
	public void removeAuthor(Long id){
		Author author = getAuthor(id);
		if (author == null)
			throw new IllegalArgumentException("Unknown author " + id);
		for(Book b : author.getBooks()){
			booksDB.remove(b.getId());
		}
	}

}
