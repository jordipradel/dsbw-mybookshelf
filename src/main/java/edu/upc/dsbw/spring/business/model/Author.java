package edu.upc.dsbw.spring.business.model;

import java.util.LinkedList;
import java.util.List;

public class Author {

	private String firstName;
	private String lastName;
	private List<Book> books = new LinkedList<Book>(); 
	
	public Author(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Book newBook(String title, int publicationYear) {
		Book result = new Book(this, title, publicationYear);
		books.add(result);
		return result;
	}
	
	void removeBook(Book book) {
		this.books.remove(book);
	}

	void addBook(Book book) {
		this.books.remove(book);
	}

	public String getName(){
		return firstName + " " + lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public List<Book> getBooks() {
		return books;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getName();
	}

}
