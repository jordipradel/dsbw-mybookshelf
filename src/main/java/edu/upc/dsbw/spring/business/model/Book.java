package edu.upc.dsbw.spring.business.model;


public class Book {

	private Long id;
	private Author author;
	private String title;
	private int publicationYear;

	Book(Author author, String title, int publicationDate) {
		this.author = author;
		this.title = title;
		this.publicationYear = publicationDate;
	}

	public void remove() {
		author.removeBook(this);
	}

	public void update(Author author, String title, int publicationDate) {
		if(!this.author.equals(author)){
			this.author.removeBook(this);
			this.author = author;
			author.addBook(this);
		}
		this.title = title;
		this.publicationYear = publicationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		if(this.id!=null && this.id != id){
			throw new IllegalStateException();
		}
		this.id = id;
		
	}

	public String getTitle() {
		return title;
	}

	public Author getAuthor() {
		return author;
	}

	public int getPublicationYear() {
		return publicationYear;
	}

}
