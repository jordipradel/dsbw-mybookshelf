package edu.upc.dsbw.spring.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.upc.dsbw.spring.business.BooksController.ListBooksOrderBy;
import edu.upc.dsbw.spring.business.model.Book;

@Controller
@RequestMapping("/books/*")
public class BooksController {
	
	@Autowired edu.upc.dsbw.spring.business.BooksController domainController;

	@RequestMapping(value="/by/{orderBy}",method=RequestMethod.GET)
	public ModelAndView listBooks(@PathVariable("orderBy") ListBooksOrderBy orderBy){
		List<Book> books = domainController.listBooks(orderBy);
		return new ModelAndView("books/list","books",books);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public ModelAndView getBook(@PathVariable("id") Long id){
		Book book = domainController.getBook(id);
		return new ModelAndView("books/detail","book",book);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.POST)
	public ModelAndView editBook(@PathVariable("id") Long id, @RequestParam("authorId") Long authorId, @RequestParam("title") String title, @RequestParam("publicationYear") int publicationYear){
		domainController.updateBook(id, authorId, title, publicationYear);
		return new ModelAndView("redirect:"+id);
	}
	
	
}
