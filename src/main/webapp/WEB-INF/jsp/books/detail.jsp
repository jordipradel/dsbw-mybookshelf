<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Books list</title>
</head>
<body>
	<h1>Book</h1>
	<label>Author: </label><p><c:out value="${book.author.name}"/></p>
	<label>Title: </label><p><c:out value="${book.title}"/></p>
	<label>Publication year: </label><p><c:out value="${book.publicationYear}"/></p>
</body>
</html>