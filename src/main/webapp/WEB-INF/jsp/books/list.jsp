<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Books list</title>
</head>
<body>
	<h1>Books</h1>
	<table>
		<thead>
			<tr><th>Author</th><th>Title</th><th>Publication</th><th></th></tr>
		</thead>
		<tbody>
			<c:forEach items="${books}" var="book">
				<tr>
				<td><c:out value="${book.author.name}"/></td>				
				<td><c:out value="${book.title}"/></td>				
				<td><c:out value="${book.publicationYear}"/></td>
				<td>
					<a href="<c:out value="/spring/books/${book.id}.html"/>">Detail</a>
				</td>
				</tr>				
			</c:forEach>
		</tbody>
	</table>
</body>
</html>