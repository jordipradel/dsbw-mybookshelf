# Laboratori de DSBW
## Entorn de desenvolupament
Per a treballar en aquests laboratoris heu de tenir instal·lat:

- Java
- [GIT](http://book.git-scm.com/2_installing_git.html)
- [Maven](http://maven.apache.org/download.html)
- Eclipse
	- L'extensió [EGit](http://www.eclipse.org/egit/) pot ser interessant

## Començar
Aquests són els passos que cal seguir per a començar a treballar com a grup:

- Crear un compte a [BitBucket](http://bitbucket.org/) per a cada component del grup
- Un dels components: Fer un Fork del repositori [dsbw-mybookshelf](https://bitbucket.org/jordipradel/dsbw-mybookshelf/)
	- Anar a https://bitbucket.org/jordipradel/dsbw-mybookshelf/
	- Seleccionar fork
	- Donar el nom del grup com a nom del repositori i marcar-lo com a privat
	- Anar a Admin
	- Donar accés a tots els components del grup i a Jordi Pradel
- Cada component: Fer un clone del repositori fent servir HTTPS per a indicar el nom d'usuari

	git clone https://*nomUsuari*@bitbucket.org/*nomPropietari*/*nomGrup*.git

## Executar l'aplicació
Per a provar l'aplicació, des de qualsevol entorn de treball, només cal que feu:

	mvn package
	java -jar target/dependency/jetty-runner.jar target/**.war
	
## Base de dades
Inicialment no farem servir base de dades. Però en cas que en vulgueu començar a fer servir, teniu, cada grup, una base de dades a la que podeu accedir des de qualsevol lloc:

- Tipus de base de dades: MySQL
- Host: dsbw.agilogy.com
- Usuari: dsbwN (on N és el número de grup)
- Contrasenya: ****** (demaneu-me-la)

## Backlog
### Rols

- Visitant: No s'ha registrat al sistema i, per tant, no està identificat. És qualsevol altre usuari.
- Usuari registrat: S'ha identificat al sistema però no és un Administrador.
- Administrador: Té un rol especial per a fer tasques d'administració

### Històries d'usuari
- Com Visitant vull poder veure la llista de novetats (llibres ordenats per data en que s'han afegit al sistema)
- Com a Visitant vull poder seleccionar un llibre i veure'n els detalls, incloent-hi la puntuació mitja
- Com a Visitant vull poder seleccionar un autor i veure quins altres llibres ha escrit
- Com a Usuari vull poder identificar-me al sistema i que em mostri els llibres que estic llegint o he llegit
- Com a Usuari vull poder indicar que he llegit o estic llegint un llibre
- Com a Visitant vull poder-me registrar com a usuari donant només un nom i una contrassenya
- Com a Visitant vull poder consultar quins llibres està llegint i quins ha llegit un altre Usuari
- Com a Usuari vull poder donar una puntuació d'1 a 5 estrelles d'un llibre que he llegit o estic llegint
- Com a Visitant vull poder veure quantes puntuacions de cada (1 a 5 estrelles) té un llibre quan en consulto els detalls
- Com a Visitant vull poder cercar un llibre introduïnt un fragment del nom d'un dels autors o del títol
- Com a Visitant vull poder ordenar el llistat de llibres cercats per popularitat, per puntuació, per odre alfabètic del títol, per autors o per editorial
- Com a Usuari vull una xarxa social de llibres on poder seguir altres usuaris
- Com a Usuari vull poder escriure ressenyes de llibres
- Com a Administrador vull poder moderar els continguts introduits pels usuaris